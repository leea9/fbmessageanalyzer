﻿import string
import pygal
import time
from pygal import style
from html.parser import HTMLParser
from datetime import datetime
from collections import OrderedDict
from bs4 import BeautifulSoup
import re

# Change Names to match your conversation
me = "Alvin Lee"
her = "May-May On"

STYLE = style.LightGreenStyle

class MLStripper(HTMLParser):
    def __init__(self):
        super().__init__()
        self.reset()
        self.strict = False
        self.convert_charrefs= True
        self.fed = []

    def handle_data(self, d):
        self.fed.append(d)

    def get_data(self):
        return self.fed

def strip_tags(html):
    s = MLStripper()
    s.feed(html)
    return s.get_data()

class message:
    def __init__(self, ts):
        self.ts = ts
        self.msg = []
        self.is_sticker = False
    
    def add_msg(self, msg):
        self.msg.append(msg)

    def num_message(self):
        return len(self.msg)

    def __radd__(self, other):
        return other + len(self.msg)

    def __lt__(self, other):
        return self.ts < other.ts

# Stickers don't get saved in this version
def count_stickers(messages):
    count = 0
    for m in messages:
        if m.is_sticker:
            count += 1
    return count

# Parses the chat log into separate lists for analysis
def parse_messages(stripped_convo):
    date_found = False
    end = len(stripped_convo)
    x = 0
    my_messages = []
    her_messages = []
    while x < end:
        line = stripped_convo[x]
        current_name = None
        try:
            timestamp = datetime.strptime(line, "%Y-%m-%d %H:%M")
            x += 1
            name = stripped_convo[x]
            x += 1
            new_message = message(timestamp)
            while True:
                try:
                    timestamp_check = datetime.strptime(stripped_convo[x], "%Y-%m-%d %H:%M")
                    # If no messages are found, it's a sticker since they don't get saved
                    if len(new_message.msg) == 0:
                        new_message.is_sticker = True
                    if name == me:
                        my_messages.append(new_message)
                    elif name == her:
                        her_messages.append(new_message)
                    break
                except ValueError:
                    pass
                if name == me:
                    new_message.add_msg(stripped_convo[x])
                    x += 1
                elif name == her:
                    new_message.add_msg(stripped_convo[x])
                    x += 1
        except:
            x += 1

    print("Length of my message: {}".format(len(my_messages)))
    print("Length of {}'s message: {}".format(her.split(" ")[0], len(her_messages)))

    return (my_messages, her_messages)

def avg_message_dow(my_messages, her_messages):
    dow_chart = pygal.Bar(legend_box_size=12, margin=20, truncate_legend=20, style=STYLE)
    dow_chart.title = "Average Messages By Day of The Week"

    dow_chart.x_labels = ['Mon', 'Tues', 'Wed', 'Thurs', 'Fri', 'Sat', 'Sun']

    current_dow = 0
    num_dow ={}
    my_dow_dict = {}
    for m in my_messages:
        dow = m.ts.weekday()
        # Keep count of the amount of day of the week for average count
        if dow != current_dow:
            if dow in num_dow:
                num_dow[dow] += 1
            else:
                num_dow[dow] = 1
            current_dow = dow

        if dow in my_dow_dict:
            my_dow_dict[dow] += m.num_message()
        else:
            my_dow_dict[dow] = m.num_message()

    her_dow_dict = {}
    for m in her_messages:
        dow = m.ts.weekday()
        if dow in her_dow_dict:
            her_dow_dict[dow] += m.num_message()
        else:
            her_dow_dict[dow] = m.num_message()

    dow_chart.add("{}".format(me.split(" ")[0]), [int((my_dow_dict[k] / num_dow[k])) for k in sorted(my_dow_dict)])
    dow_chart.add("{}".format(her.split(" ")[0]), [int((her_dow_dict[k] / num_dow[k])) for k in sorted(her_dow_dict)])

    dow_chart.render_to_file("svg/dow_messages.svg")

def message_over_time(my_messages, her_messages):
    mot_chart = pygal.DateTimeLine(x_label_rotation=35, x_value_formatter=lambda dt: dt.strftime('%Y-%m-%d'), style=STYLE)
    mot_chart.title = "Messages Over Time By Day"
    
    my_mot_data = OrderedDict()
    for m in my_messages:
        just_day = m.ts.replace(hour=0, minute=0)
        if just_day in my_mot_data:
            my_mot_data[just_day] += m.num_message()
        else:
            my_mot_data[just_day] = m.num_message()

    her_mot_data = OrderedDict()
    for m in her_messages:
        just_day = m.ts.replace(hour=0, minute=0)
        if just_day in her_mot_data:
            her_mot_data[just_day] += m.num_message()
        else:
            her_mot_data[just_day] = m.num_message()

    mot_chart.add(me.split(" ")[0], list(my_mot_data.items()))
    mot_chart.add(her.split(" ")[0], list(her_mot_data.items()))
    mot_chart.render_to_file("svg/mot_messages.svg")

def message_by_hour_of_day(my_message, her_messages):
    moh_chart = pygal.Bar(legend_box_size=12, margin=0, style=STYLE)
    moh_chart.title = "Messages By Hour Of The Day"

    moh_chart.x_labels = map(str, range(0, 24))

    my_hour_dict = {}
    for m in my_messages:
        hr = m.ts.hour
        if hr in my_hour_dict:
            my_hour_dict[hr] += m.num_message()
        else:
            my_hour_dict[hr] = m.num_message()

    her_hour_dict = {}
    for m in her_messages:
        hr = m.ts.hour
        if hr in her_hour_dict:
            her_hour_dict[hr] += m.num_message()
        else:
            her_hour_dict[hr] = m.num_message()

    # Make sure all the hours are in the dictionary to prevent bad data
    for x in range(0, 24):
        if x not in my_hour_dict:
            my_hour_dict[x] = 0
        if x not in her_hour_dict:
            her_hour_dict[x] = 0

    moh_chart.add("{}".format(me.split(" ")[0]), [my_hour_dict[k] for k in sorted(my_hour_dict)])
    moh_chart.add("{}".format(her.split(" ")[0]), [her_hour_dict[k] for k in sorted(her_hour_dict)])

    moh_chart.render_to_file("svg/moh_messages.svg")

# The first 2 arguments must be integer numbers not lists
def create_pie_chart(my_message, her_message, title, file_name):
    pie_chart = pygal.Pie(legend_box_size=12, margin=20, truncate_legend=20, style=STYLE)
    pie_chart.title = title

    pie_chart.add("{} - {}".format(me.split(" ")[0], my_message), my_message)
    pie_chart.add("{} - {}".format(her.split(" ")[0], her_message), her_message)

    pie_chart.render_to_file("svg/{}.svg".format(file_name))

class FbMessage:
    def __init__(self, ts, nm, msg, st, em):
        self.timestamp = ts
        self.name = nm
        self.message = msg
        self.stickers = st
        self.emoticon = em

if __name__ == "__main__":
    '''
        Assumes messages of format:
        <date [YYYY-MM-DD HH:MM]>
        <name>
        <message_line>
        <message_line>
        ...
        <n_message_line>
    '''

    message_list = []
    with open("Fb_message_full.htm", "r", encoding='utf-8') as file:
        start = time.time()
        convo = BeautifulSoup(file.read(), "lxml")
        end = time.time()
        print("Time it took to parse xml: {0:.3f}s".format(end - start))

    convos = convo.find_all('li', class_="webMessengerMessageGroup clearfix")
    start = time.time()
    for c in convos:
        timestamp = c.find("abbr", {"class": "_35 timestamp"})
        name = c.find("strong", {"class": "_36"})
        messages = [t.text for t in c.find_all("p", class_="pClass")]
        emoticon = c.find_all("span", class_="emoticon")
        sticker = c.find("div", class_="_sq")
        message_list.append(FbMessage(timestamp, name, messages, sticker, emoticon))
    
    end = time.time()
     
    print("Time it took to parse messages: {0:.3f}s".format(end - start))
    print("stop")
    
    '''
    my_messages, her_messages = parse_messages(stripped_convo)

    my_stickers = count_stickers(my_messages)
    her_stickers = count_stickers(her_messages)

    my_lines  = sum(my_messages)
    her_lines = sum(her_messages)

    
    create_pie_chart(my_stickers, her_stickers, "Stickers Sent (Total: {})".format(my_stickers + her_stickers), file_name="stickers")
    
    create_pie_chart(my_lines + my_stickers, her_lines + her_stickers, 
                     "Messages Sent (Total: {})".format(my_lines + her_lines + my_stickers + her_stickers), 
                     file_name="total_lines")
    avg_message_dow(my_messages, her_messages)
    
    message_over_time(my_messages, her_messages)

    message_by_hour_of_day(my_messages, her_messages)
    '''